<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<!--[if IE 6]>
<link rel="stylesheet" href="<?php print $base_path . $directory ?>/fix.css" type="text/css" />
<![endif]-->
</head>

<body>
<div id="sidebar">
<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><h1><?php print $site_name ?></h1></a>
<h2><?php print $site_slogan ?></h2>

<div id="menu">
<?php if (is_array($primary_links)) :
	foreach ($primary_links as $link):
		print $link;
	endforeach;
endif; ?>
</div>
<?php print $sidebar_left ?>
<?php print $sidebar_right ?>
</div>

<div id="content">
	<?php print $header ?>
	<h1><?php print $site_name ?></h1>
	<h2><?php print $site_slogan ?></h2>
	<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
	<?php print $breadcrumb ?>
	<h3><?php print $title ?></h3>
	<div class="tabs"><?php print $tabs ?></div>
	<?php print $help ?>
	<?php print $messages ?>
	<?php print $content; ?>
	<h3>site info</h3>
	<?php print $footer_message ?>
</div>
<?php print $closure ?>
</body>
</html>